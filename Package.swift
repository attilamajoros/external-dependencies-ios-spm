// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Externals",
	platforms: [
		.macOS(.v10_14), .iOS(.v13), .tvOS(.v13)
	],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "Externals",
            targets: ["Chartbeat","ChoiceMobile","ComScore","Dotmetrics","DTBiOSSDK",
					  "FBLPromises","FirebaseABTesting","FirebaseAnalytics",
					  "FirebaseAuth","FirebaseCore","FirebaseCoreDiagnostics","FirebaseCrashlytics",
					  "FirebaseInstallations","FirebaseRemoteConfig","GoogleAnalytics","GoogleAppMeasurement",
					  "GoogleDataTransport","GoogleInteractiveMediaAds","GoogleMobileAds","GoogleSymbolUtilities",
					  "GoogleTagManager","GoogleUtilities","GoogleUtilitiesLegacy","GTMSessionFetcher",
					  "nanopb","TaboolaSDK","UserMessagingPlatform"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
		
		.binaryTarget(name: "Chartbeat", path: "./Sources/Chartbeat/Chartbeat.xcframework"),
		.binaryTarget(name: "ChoiceMobile", path: "./Sources/ChoiceMobile/ChoiceMobile.xcframework"),
		.binaryTarget(name: "ComScore", path: "./Sources/ComScore/ComScore.xcframework"),
		.binaryTarget(name: "Dotmetrics", path: "./Sources/Dotmetrics/Dotmetrics.xcframework"),
		.binaryTarget(name: "DTBiOSSDK", path: "./Sources/DTBiOSSDK/DTBiOSSDK.xcframework"),
		.binaryTarget(name: "FBLPromises", path: "./Sources/FBLPromises/FBLPromises.xcframework"),
		.binaryTarget(name: "FirebaseABTesting", path: "./Sources/FirebaseABTesting/FirebaseABTesting.xcframework"),
		.binaryTarget(name: "FirebaseAnalytics", path: "./Sources/FirebaseAnalytics/FirebaseAnalytics.xcframework"),
		.binaryTarget(name: "FirebaseAuth", path: "./Sources/FirebaseAuth/FirebaseAuth.xcframework"),
		.binaryTarget(name: "FirebaseCore", path: "./Sources/FirebaseCore/FirebaseCore.xcframework"),
		.binaryTarget(name: "FirebaseCoreDiagnostics", path: "./Sources/FirebaseCoreDiagnostics/FirebaseCoreDiagnostics.xcframework"),
		.binaryTarget(name: "FirebaseCrashlytics", path: "./Sources/FirebaseCrashlytics/FirebaseCrashlytics.xcframework"),
		.binaryTarget(name: "FirebaseInstallations", path: "./Sources/FirebaseInstallations/FirebaseInstallations.xcframework"),
		.binaryTarget(name: "FirebaseRemoteConfig", path: "./Sources/FirebaseRemoteConfig/FirebaseRemoteConfig.xcframework"),
		.binaryTarget(name: "GoogleAnalytics", path: "./Sources/GoogleAnalytics/GoogleAnalytics.xcframework"),
		.binaryTarget(name: "GoogleAppMeasurement", path: "./Sources/GoogleAppMeasurement/GoogleAppMeasurement.xcframework"),
		.binaryTarget(name: "GoogleDataTransport", path: "./Sources/GoogleDataTransport/GoogleDataTransport.xcframework"),
		.binaryTarget(name: "GoogleInteractiveMediaAds", path: "./Sources/GoogleInteractiveMediaAds/GoogleInteractiveMediaAds.xcframework"),
		.binaryTarget(name: "GoogleMobileAds", path: "./Sources/GoogleMobileAds/GoogleMobileAds.xcframework"),
		.binaryTarget(name: "GoogleSymbolUtilities", path: "./Sources/GoogleSymbolUtilities/GoogleSymbolUtilities.xcframework"),
		.binaryTarget(name: "GoogleTagManager", path: "./Sources/GoogleTagManager/GoogleTagManager.xcframework"),
		.binaryTarget(name: "GoogleUtilities", path: "./Sources/GoogleUtilities/GoogleUtilities.xcframework"),
		.binaryTarget(name: "GoogleUtilitiesLegacy", path: "./Sources/GoogleUtilitiesLegacy/GoogleUtilitiesLegacy.xcframework"),
		.binaryTarget(name: "GTMSessionFetcher", path: "./Sources/GTMSessionFetcher/GTMSessionFetcher.xcframework"),
		.binaryTarget(name: "nanopb", path: "./Sources/nanopb/nanopb.xcframework"),
		.binaryTarget(name: "TaboolaSDK", path: "./Sources/TaboolaSDK/TaboolaSDK.xcframework"),
		.binaryTarget(name: "UserMessagingPlatform", path: "./Sources/UserMessagingPlatform/UserMessagingPlatform.xcframework"),
    ]
)
